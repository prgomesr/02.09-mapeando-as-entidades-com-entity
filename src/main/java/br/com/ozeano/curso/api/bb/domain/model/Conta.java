package br.com.ozeano.curso.api.bb.domain.model;

import javax.persistence.Entity;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Conta extends BaseEntity {

	private String agencia;
	private String conta;
	private String digitoAgencia;
	private String digitoConta;
	private Banco banco;
	private Empresa empresa;
	
}
