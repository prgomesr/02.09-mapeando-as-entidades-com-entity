package br.com.ozeano.curso.api.bb.domain.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Fatura extends BaseEntity {

	private BigDecimal valor;
	private LocalDate dataVencimento;
	private TipoFatura tipo;
	private TipoPagamento tipoPagamento;
	private SituacaoFatura situacao;
	private String numeroDocumento;
	private String nossoNumero;
	private Conta conta;
	private Convenio convenio;
	private Pessoa pessoa;

}
